/**
 * Interface for drawing the state
 */
public interface IStateDrawer {

    void draw();

    void drawNext(int slot, int orient);

    //visualization
    //clears the area where the next piece is shown (top)
    void clearNext();

    TLabel getLabel();

    void setLabel(TLabel label);
}
