
public interface IState extends IStateTemplate, IStateDrawer {

    int[][] getField();

    int[] getTop();

    int getNextPiece();

    boolean hasLost();

    int getRowsCleared();

    int getTurnNumber();

    //gives legal moves for nextPiece
    int[][] legalMoves();

    //make a move based on the move index - its order in the legalMoves list
    void makeMove(int move);

    //make a move based on an array of orient and slot
    void makeMove(int[] move);

    //returns false if you lose - true otherwise
    boolean makeMove(int orient, int slot);


}
