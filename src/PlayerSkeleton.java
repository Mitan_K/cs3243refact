import java.util.stream.IntStream;

public class PlayerSkeleton implements IPlayerSkeleton {

    private static final int DELAY_PLACE = 1;
    private static final Heuristics HEURISTICS_DEFAULT = new Heuristics(
            3.2964451103883965, 3.5899795568015964, 1.0524013019032903, 3.7820416035881808, 8.487428824130264, 1.8307396428689233
            );

    // implement this function to have a working system
    @Override
    public int pickMove(IState s, int[][] legalMoves) {
        AnalysisResult winningMove = IntStream.range(0, legalMoves.length).parallel()
            .mapToObj(moveId -> new StateAnalyser(s, moveId))
            .map(StateAnalyser::analyse)
            .filter(move -> !move.isLosingMove())
            .max((a1, a2) -> HEURISTICS_DEFAULT.calculate(a1).compareTo(HEURISTICS_DEFAULT.calculate(a2)))
            .orElse(null);
        if (winningMove == null) return 0;
        return winningMove.getMoveIndex();
    }

    private class StateAnalyser {

        /**
         * Fields
         */
        private final IState _state;
        private final int _piece;

        private final int _moveId;
        private final int _orient;
        private final int _slot;

        private final int _base;

        /**
         * Constructs a new field analyst
         */
        public StateAnalyser(IState state, int moveId) {
            _state = state;
            _piece = state.getNextPiece();

            _moveId = moveId;
            _orient = state.getAllLegalMoves()[_piece][moveId][State.ORIENT];
            _slot = state.getAllLegalMoves()[_piece][moveId][State.SLOT];

            _base = this.getBase();
        }

        public AnalysisResult analyse() {
            // Prepare results
            final int[] heuristics = new int[Heuristics.COUNT];

            // Get height of new move
            int maxHeight = IntStream.range(0, getPieceWidth())
                    .map(pieceCol -> pieceCol + _slot)
                    .map(this::getVirtualHeight)
                    .max().getAsInt();
            if (maxHeight >= State.ROWS) {
                return AnalysisResult.losingMove(_moveId);
            }

            // LANDING HEIGHT
            heuristics[Heuristics.ID_LANDING_HEIGHT] = this.calculateLandingHeight();

            // ROWS ELIMINATED
            heuristics[Heuristics.ID_ROWS_ELIMINATED] = this.calculateRowsEliminated();

            // ROW TRANSITIONS
            heuristics[Heuristics.ID_ROW_TRANSITIONS] = this.calculateRowTransitions();

            // COL TRANSITIONS
            heuristics[Heuristics.ID_COLUMN_TRANSITIONS] = this.calculateColTransitions();

            // NUMBER OF HOLES
            heuristics[Heuristics.ID_HOLES_COUNT] = this.calculateHolesCount();

            // WELL SUMS
            heuristics[Heuristics.ID_WELL_SUMS] = this.calculateWellSums();

            return new AnalysisResult(_moveId, heuristics);
        }

        private int getBase() {
            return IntStream.range(0, getPieceWidth())
                    .map(index -> _state.getTop()[_slot + index] - getPieceBottom(index))
                    .max().orElse(0);
        }

        private int calculateLandingHeight() {
            return _base + (this.getPieceHeight() - 1) / 2;
        }

        private int calculateRowsEliminated() {
            int lowestRow = IntStream.range(0, State.COLS)
                    .map(this::getVirtualHeight)
                    .min().getAsInt();
            return (int) IntStream.range(0, lowestRow)
                    .filter(row -> IntStream.range(0, State.COLS)
                            .filter(col -> hasCellAt(row, col))
                            .count() == 0)
                    .count();
        }

        private int calculateRowTransitions() {
            int highestRow = IntStream.range(0, State.COLS)
                    .map(this::getVirtualHeight)
                    .max().getAsInt();
            return IntStream.range(0, highestRow)
                    .map(row -> {
                        int transitions = 0;
                        for (int col = 1; col < State.COLS; col++) {
                            if (hasCellAt(row, col) ^ hasCellAt(row, col-1)) {
                                transitions++;
                            }
                        }
                        return transitions;
                    })
                    .sum();
        }

        private int calculateColTransitions() {
            return IntStream.range(0, State.COLS)
                    .map(col -> {
                        int transitions = 0;
                        int rowHeight = getVirtualHeight(col);
                        for (int row = 0; row <= rowHeight; row++) {
                            if (hasCellAt(row, col) ^ hasCellAt(row + 1, col)) {
                                transitions++;
                            }
                        }
                        return transitions;
                    })
                    .sum();
        }

        private int calculateHolesCount() {
            return IntStream.range(0, State.COLS)
                    .map(col -> {
                        int holes = 0;
                        int rowHeight = getVirtualHeight(col);
                        for (int row = 0; row < rowHeight - 1; row++) {
                            if (!hasCellAt(row, col)) {
                                holes++;
                            }
                        }
                        return holes;
                    })
                    .sum();
        }

        private int calculateWellSums() {
            int innerWellSums = IntStream.range(1, State.COLS - 1)
                    .map(col -> {
                        int wellSize = 0;
                        int rowHeight = getVirtualHeight(col);
                        for (int row = rowHeight; row < State.ROWS; row++) {
                            if (!hasCellAt(row, col) && hasCellAt(row, col - 1) && hasCellAt(row, col + 1)) {
                                wellSize++;

                                // Keep counting from this cell downwards
                                for (int i = row - 1; i >= 0; i--) {
                                    if (!hasCellAt(i, col)) {
                                        wellSize++;
                                    } else {
                                        break;
                                    }
                                }
                            }
                        }
                        return wellSize;
                    }).sum();

            int leftWellSums = 0;
            int firstColHeight = getVirtualHeight(0);
            for (int row = firstColHeight; row < State.ROWS; row++) {
                if (!hasCellAt(row, 0) && hasCellAt(row, 1)) {
                    leftWellSums++;

                    // Keep counting from this cell downwards
                    for (int i = row - 1; i >= 0; i--) {
                        if (!hasCellAt(i, 0)) {
                            leftWellSums++;
                        } else {
                            break;
                        }
                    }
                }
            }

            int rightWellSums = 0;
            int lastCol = State.COLS - 1;
            int lastColHeight = getVirtualHeight(lastCol);
            for (int row = lastColHeight; row < State.ROWS; row++) {
                if (!hasCellAt(row, lastCol) && hasCellAt(row, lastCol - 1)) {
                    rightWellSums++;

                    // Keep counting from this cell downwards
                    for (int i = row - 1; i >= 0; i--) {
                        if (!hasCellAt(i, lastCol)) {
                            rightWellSums++;
                        } else {
                            break;
                        }
                    }
                }
            }

            return innerWellSums + leftWellSums + rightWellSums;
        }

        private int getPieceTop(int pieceCol) {
            return State.pTop[_piece][_orient][pieceCol];
        }

        private int getPieceBottom(int pieceCol) {
            return State.pBottom[_piece][_orient][pieceCol];
        }

        private int getPieceWidth() {
            return State.pWidth[_piece][_orient];
        }

        private int getPieceHeight() {
            return State.pHeight[_piece][_orient];
        }

        private int getVirtualHeight(int column) {
            if (!isAffectedColumn(column)) {
                return _state.getTop()[column];
            }
            return _base + getPieceTop(column - _slot);
        }

        private boolean hasCellAt(int row, int col) {
            // More than height, nothing is here
            if (row >= getVirtualHeight(col)) {
                return false;
            }
            // If the old field has it there, it will still be there
            // in the context of this function
            if (_state.getField()[row][col] > 0) {
                return true;
            }
            // If outside of the affected area, and still it has nothing
            // here, then it will not have anything there anyway
            if (!isAffectedColumn(col) || row < _base) {
                return false;
            }

            // Now we can besure that it's within affected area
            col -= _slot;
            row -= _base;
            return row >= getPieceBottom(col);
        }

        private boolean isAffectedColumn(int column) {
            return column >= _slot && column < _slot + getPieceWidth();
        }
    }

    private static class Heuristics {

        /**
         * Constants
         */

        public static final double LOWER_BOUND = 0.0;
        public static final double UPPER_BOUND = 10.0;

        public static final int COUNT = 6;
        public static final int ID_LANDING_HEIGHT = 0;
        public static final int ID_ROWS_ELIMINATED = 1;
        public static final int ID_ROW_TRANSITIONS = 2;
        public static final int ID_COLUMN_TRANSITIONS = 3;
        public static final int ID_HOLES_COUNT = 4;
        public static final int ID_WELL_SUMS = 5;

        /**
         * Properties
         */
        private double[] _coefficients;

        public Heuristics(double... coefficients) {
            assert coefficients.length == COUNT;
            this._coefficients = coefficients;
        }

        public double[] getValues() {
            return this._coefficients;
        }

        public void setValues(double... coefficients) {
            this._coefficients = coefficients;
        }

        public Double calculate(AnalysisResult result) {
            return -this._coefficients[ID_LANDING_HEIGHT] * result.getLandingHeight() +
                    this._coefficients[ID_ROWS_ELIMINATED] * result.getRowsEliminated() +
                    -this._coefficients[ID_ROW_TRANSITIONS] * result.getRowTransitions() +
                    -this._coefficients[ID_COLUMN_TRANSITIONS] * result.getColTransitions() +
                    -this._coefficients[ID_HOLES_COUNT] * result.getHolesCount() +
                    -this._coefficients[ID_WELL_SUMS] * result.getWellSums();
        }
    }

    private static class AnalysisResult {

        /**
         * Properties
         */
        private final int _moveIndex;
        private final int[] _heuristicValues;
        private final boolean _isLosingMove;

        /**
         * Constructs a new analysis result from the given calculated values
         * @param moveIndex the index of the move made
         * @param heuristicValues
         */
        public AnalysisResult(int moveIndex, int... heuristicValues) {
            assert heuristicValues.length == Heuristics.COUNT;
            this._moveIndex = moveIndex;
            this._heuristicValues = heuristicValues;
            this._isLosingMove = false;
        }

        /**
         * Getters
         */
        public int getMoveIndex() {
            return this._moveIndex;
        }

        public int getLandingHeight() {
            assert this._heuristicValues != null;
            return this._heuristicValues[Heuristics.ID_LANDING_HEIGHT];
        }

        public int getRowsEliminated() {
            assert this._heuristicValues != null;
            return this._heuristicValues[Heuristics.ID_ROWS_ELIMINATED];
        }

        public int getRowTransitions() {
            assert this._heuristicValues != null;
            return this._heuristicValues[Heuristics.ID_ROW_TRANSITIONS];
        }

        public int getColTransitions() {
            assert this._heuristicValues != null;
            return this._heuristicValues[Heuristics.ID_COLUMN_TRANSITIONS];
        }

        public int getHolesCount() {
            assert this._heuristicValues != null;
            return this._heuristicValues[Heuristics.ID_HOLES_COUNT];
        }

        public int getWellSums() {
            assert this._heuristicValues != null;
            return this._heuristicValues[Heuristics.ID_WELL_SUMS];
        }

        // Losing move constructor
        private AnalysisResult(int moveId) {
            this._moveIndex = moveId;
            this._isLosingMove = true;
            this._heuristicValues = null;
        }

        public static AnalysisResult losingMove(int moveId) {
            return new AnalysisResult(moveId);
        }

        public boolean isLosingMove() {
            return this._isLosingMove;
        }
    }
}
