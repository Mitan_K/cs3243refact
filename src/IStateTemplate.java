import java.awt.*;

/**
 * Interface for storing the const parameters of the field
 */
public interface IStateTemplate {
    int COLS = 10;
    int ROWS = 21;
    // number of different pieces
    int N_PIECES = 7;

    //indices for legalMoves
    int ORIENT = 0;
    int SLOT = 1;
    //possible orientations for a given piece type
    int[] pOrients = {1,2,4,4,4,2,2};
    //the next several arrays define the piece vocabulary in detail
    //width of the pieces [piece ID][orientation]
    int[][] pWidth = {
            {2},
            {1,4},
            {2,3,2,3},
            {2,3,2,3},
            {2,3,2,3},
            {3,2},
            {3,2}
    };
    //height of the pieces [piece ID][orientation]
    int[][] pHeight = {
            {2},
            {4,1},
            {3,2,3,2},
            {3,2,3,2},
            {3,2,3,2},
            {2,3},
            {2,3}
    };
    int[][][] pBottom = {
        {{0,0}},
        {{0},{0,0,0,0}},
        {{0,0},{0,1,1},{2,0},{0,0,0}},
        {{0,0},{0,0,0},{0,2},{1,1,0}},
        {{0,1},{1,0,1},{1,0},{0,0,0}},
        {{0,0,1},{1,0}},
        {{1,0,0},{0,1}}
    };
    int[][][] pTop = {
        {{2,2}},
        {{4},{1,1,1,1}},
        {{3,1},{2,2,2},{3,3},{1,1,2}},
        {{1,3},{2,1,1},{3,3},{2,2,2}},
        {{3,2},{2,2,2},{2,3},{1,2,1}},
        {{1,2,2},{3,2}},
        {{2,2,1},{2,3}}
    };Color brickCol = Color.gray;

    //all legal moves - first index is piece type - then a list of 2-length arrays
    default int[][][] getAllLegalMoves() {

        int[][][] legalMoves = new int[N_PIECES][][];

        {
            //for each piece type
            for (int i = 0; i < N_PIECES; i++) {
                //figure number of legal moves
                int n = 0;
                for (int j = 0; j < pOrients[i]; j++) {
                    //number of locations in this orientation
                    n += COLS + 1 - pWidth[i][j];
                }
                //allocate space
                legalMoves[i] = new int[n][2];
                //for each orientation
                n = 0;
                for (int j = 0; j < pOrients[i]; j++) {
                    //for each slot
                    for (int k = 0; k < COLS + 1 - pWidth[i][j]; k++) {
                        legalMoves[i][n][ORIENT] = j;
                        legalMoves[i][n][SLOT] = k;
                        n++;
                    }
                }
            }

        }
        return legalMoves;
    }
}
