/**
 * Created by a0134673 on 2/23/2017.
 */
public interface IPlayerSkeleton {
    // implement this function to have a working system
    int pickMove(IState s, int[][] legalMoves);
}
